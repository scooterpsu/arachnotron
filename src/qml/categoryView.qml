import QtQuick 2.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/categories.js" as Categories
import "../scripts/categoryView.js" as CategoryView

Item {
    id: element
    width: 888
    height: 480

    Text {
        id: categoryTitle
        color: "#ffffff"
        text: Categories.selectedCategory.getString("name") + " (" + Categories.selectedCategory.loadProfiles() + ")"
        anchors.top: parent ? parent.top : undefined
        anchors.topMargin: 5
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 31
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 0
        font.pixelSize: 25
        font.family: Arachnotron.bigFont
    }

    ScrollView {
        id: profilesView
        topPadding: 1
        bottomPadding: 30
        leftPadding: 20
        anchors.top: categoryTitle.bottom
        anchors.right: parent ? parent.right : undefined
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.left: parent ? parent.left : undefined
        anchors.topMargin: 5
        anchors.leftMargin: -20
        contentWidth: parent ? parent.width : undefined
        clip: true
        ScrollBar.horizontal.active: false

        Flow {
            id: profilesLayout
            width: parent.width - 5
            x: 0
            spacing: 16

            Component.onCompleted: CategoryView.setProfileListTarget(this)
        }
    }
}
