import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import Arachnotron 1.0
import "../scripts/profiles.js" as Profiles
import "../scripts/fileLoader.js" as FileLoader
import "../scripts/editor.js" as Editor

Item {
    id: profileView
    width: 888
    height: 1280

    property var title: profileTitle
    property var saveData: Editor.saveData

    Text {
        id: profileTitle
        x: 20
        color: "#ffffff"
        text: Profiles.selectedProfile.getString("name")
        anchors.top: parent ? parent.top : undefined
        anchors.topMargin: 5
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 20
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 0
        font.pixelSize: 25
        font.family: Arachnotron.bigFont
    }

    Image {
        id: panelBackground
        anchors.top: profileTitle.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 6
        anchors.leftMargin: -31
        source: Profiles.selectedProfile && Profiles.selectedProfile.getAssetPath("backgroundPath") ? Profiles.selectedProfile.getAssetPath("backgroundPath") : "../assets/ProfileViewBG.png"
        fillMode: Image.Tile

        DropShadow {
            id: contentShadow
            x: 0
            y: 0
            color: "#000000"
            radius: 8
            source: content
            anchors.fill: content
            transparentBorder: true
            horizontalOffset: 2
            samples: 17
            verticalOffset: 2
        }

        ScrollView {
            id: content
            bottomPadding: 40
            topPadding: 20
            leftPadding: 11
            anchors.fill: parent
            contentWidth: parent.width
            clip: true
            ScrollBar.horizontal.active: false

            Flow {
                id: contentLayout
                width: parent.width - 40
                x: 20
                spacing: 16

                ColumnLayout {
                    id: gameLayout
                    width: {
                        var newWidth = (parent.width / 2) - (parent.spacing / 2);
                        if(newWidth >= 416) return newWidth;
                        return parent.width;
                    }
                    spacing: 16

                    ColumnLayout {
                        id: startingLayout
                        Layout.fillWidth: true
                        spacing: 8

                        RowLayout {
                            id: mapLayout
                            spacing: 8
                            Layout.fillWidth: true

                            Text {
                                id: mapLabel
                                color: "#ffffff"
                                text: qsTr("Starting Map")
                                Layout.preferredWidth: 204
                                font.pointSize: 16
                                verticalAlignment: Text.AlignBottom
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            TextField {
                                id: mapInput
                                height: 27
                                text: ""
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                font.family: "Verdana"
                                font.pointSize: 12
                                padding: -1
                                leftPadding: 12
                                rightPadding: 4
                                topPadding: 3
                                bottomPadding: 0
                                selectByMouse: true
                                Component.onCompleted: Editor.setTextTarget("map", this, Profiles.getLaunch())
                            }
                        }

                        RowLayout {
                            id: skillLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: skillLabel
                                color: "#ffffff"
                                text: qsTr("Skill Level")
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            TextField {
                                id: skillInput
                                height: 27
                                text: ""
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                padding: -1
                                leftPadding: 12
                                topPadding: 3
                                bottomPadding: 0
                                font.family: "Verdana"
                                font.pointSize: 12
                                rightPadding: 4
                                selectByMouse: true
                                Component.onCompleted: Editor.setTextTarget("skill", this, Profiles.getLaunch())
                            }
                        }

                        RowLayout {
                            id: loadGameLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: loadGameLabel
                                color: "#ffffff"
                                text: qsTr("Load Game Save")
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            TextField {
                                id: loadGameInput
                                height: 27
                                text: ""
                                Layout.fillWidth: true
                                Layout.preferredWidth: 156
                                font.pointSize: 12
                                font.family: "Verdana"
                                padding: -1
                                leftPadding: 12
                                rightPadding: 4
                                topPadding: 1
                                bottomPadding: 0
                                selectByMouse: true
                                Component.onCompleted: Editor.setTextTarget("loadGame", this, Profiles.getLaunch())
                            }

                            Button {
                                id: loadGameButton
                                width: 40
                                height: 40
                                Layout.maximumHeight: 40
                                Layout.minimumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.minimumWidth: 40
                                font.pointSize: 16
                                icon.source: "../assets/fileIcon.svg"
                                highlighted: loadMouseCatch.containsMouse

                                MouseArea {
                                    id: loadMouseCatch
                                    anchors.fill: parent
                                    hoverEnabled: true
                                }
                            }
                        }

                        RowLayout {
                            id: playerClassLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: playerClassLabel
                                color: "#ffffff"
                                text: qsTr("Player Class")
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            ComboBox {
                                id: playerClassInput
                                height: 27
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                currentIndex: -1
                                textRole: "name"
                                model: ListModel {
                                    id: playerClassItems
                                }
                                font.family: "Verdana"
                                font.pointSize: 12
                                editable: true
                                contentItem: TextField {
                                      text: playerClassInput.editText
                                      selectByMouse: true
                                }
                                Component.onCompleted: Editor.setComboTarget("playerClass", this, Profiles.getLaunch(), Profiles.selectedProfile.getInheritedList("playerClasses"), undefined)
                            }
                        }

                        RowLayout {
                            id: noMonstersLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: noMonstersLabel
                                color: "#ffffff"
                                text: qsTr("No Monsters")
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: noMonstersInput
                                height: 27
                                checked: false
                                Component.onCompleted: Editor.setBoolTarget("noMonsters", this, Profiles.getLaunch())
                            }
                        }

                        RowLayout {
                            id: fastMonstersLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: fastMonstersLabel
                                color: "#ffffff"
                                text: qsTr("Fast Monsters")
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: fastMonstersInput
                                height: 27
                                checked: false
                                Component.onCompleted: Editor.setBoolTarget("fastMonsters", this, Profiles.getLaunch())
                            }
                        }

                        RowLayout {
                            id: respawnMonstersLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: respawnMonstersLabel
                                color: "#ffffff"
                                text: qsTr("Respawn Monsters")
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: respawnMonstersInput
                                height: 27
                                checked: false
                                Component.onCompleted: Editor.setBoolTarget("respawnMonsters", this, Profiles.getLaunch())
                            }
                        }

                        RowLayout {
                            id: freezeLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: freezeLabel
                                color: "#ffffff"
                                text: qsTr("Freeze On Start")
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: freezeInput
                                height: 27
                                checked: false
                                Component.onCompleted: Editor.setBoolTarget("freeze", this, Profiles.getLaunch())
                            }
                        }

                        RowLayout {
                            id: timerLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: timeLabel
                                color: "#ffffff"
                                text: qsTr("Next Level Time Limit")
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            TextField {
                                id: timeInput
                                height: 27
                                text: ""
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                padding: -1
                                leftPadding: 8
                                topPadding: 3
                                bottomPadding: 0
                                font.family: "Verdana"
                                font.pointSize: 12
                                rightPadding: 4
                                selectByMouse: true
                                inputMethodHints: Qt.ImhFormattedNumbersOnly
                                validator: DoubleValidator {}
                                Component.onCompleted: Editor.setDoubleTarget("timer", this, Profiles.getLaunch())
                            }
                        }
                    }

                    ColumnLayout {
                        id: filesLayout
                        Layout.fillWidth: true
                        spacing: 8

                        Text {
                            id: filesLabel
                            color: "#ffffff"
                            text: qsTr("Additional Resources")
                            Layout.fillWidth: true
                            font.family: Arachnotron.mainFont
                            font.pointSize: 16
                            verticalAlignment: Text.AlignBottom
                            horizontalAlignment: Text.AlignHCenter
                            lineHeight: 1.22
                        }

                        Rectangle {
                            id: filesBackground
                            height: 190
                            color: "#40000000"
                            Layout.fillWidth: true
                            border.color: "#00000000"
                            Component.onCompleted: Editor.setListTarget("resources", this, Profiles.getLaunch(), "../qml/listEntryFile.qml", "files", Profiles.selectedProfile.getSourceDir())
                        }
                    }

                    ColumnLayout {
                        id: cvarsLayout
                        Layout.fillWidth: true
                        spacing: 8

                        Text {
                            id: cvarsLabel
                            color: "#ffffff"
                            text: qsTr("Console Variables")
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            font.family: Arachnotron.mainFont
                            lineHeight: 1.22
                        }

                        Rectangle {
                            id: cvarsBackground
                            height: 190
                            color: "#40000000"
                            Layout.fillWidth: true
                            border.color: "#00000000"
                            Component.onCompleted: Editor.setMapTarget("cvars", this, Profiles.selectedProfile, "../qml/listEntryCvarLaunch.qml", "launchCvars", undefined, true)
                        }
                    }

                    ColumnLayout {
                        id: argsLayout
                        Layout.fillWidth: true
                        spacing: 8

                        Text {
                            id: argsLabel
                            color: "#ffffff"
                            text: qsTr("Additional Arguments")
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignBottom
                            font.family: "Verdana"
                            font.pointSize: 12
                            lineHeight: 1.22
                        }

                        TextField {
                            id: argsInput
                            height: 27
                            text: ""
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            font.pointSize: 16
                            padding: -1
                            leftPadding: 12
                            topPadding: 3
                            bottomPadding: 0
                            font.family: Arachnotron.mainFont
                            rightPadding: 4
                            selectByMouse: true
                            Component.onCompleted: Editor.setTextTarget("args", this, Profiles.getLaunch())
                        }
                    }
                }

                ColumnLayout {
                    id: multiplayerLayout
                    width: {
                        var newWidth = (parent.width / 2) - (parent.spacing / 2);
                        if(newWidth >= 416) return newWidth;
                        return parent.width;
                    }
                    spacing: 8

                    RowLayout {
                        id: multiplayerEnabledLayout
                        spacing: 8
                        width: parent.width

                        Text {
                            id: multiplayerEnabledLabel
                            color: "#ffffff"
                            text: qsTr("Multiplayer")
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Switch {
                            id: multiplayerEnabledInput
                            height: 27
                            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                            checked: false
                            Component.onCompleted: Editor.setBoolTarget("multiplayer", this, Profiles.getLaunch())
                        }
                    }

                    RowLayout {
                        id: hostLayout
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: hostLabel
                            color: "#ffffff"
                            text: qsTr("Host Game")
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Switch {
                            id: hostInput
                            height: 27
                            checked: false
                            Component.onCompleted: Editor.setBoolTarget("host", this, Profiles.getLaunch())
                        }
                    }

                    RowLayout {
                        id: joinLayout
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: joinLabel
                            color: "#ffffff"
                            text: qsTr("Join Address")
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Item {
                            id: joinInputItem
                            height: 40
                            Layout.fillWidth: true
                            Layout.preferredWidth: 122

                            TextField {
                                id: joinInput
                                text: ""
                                anchors.right: parent.right
                                anchors.left: parent.left
                                padding: -1
                                leftPadding: 12
                                topPadding: 3
                                bottomPadding: 0
                                font.family: "Verdana"
                                font.pointSize: 12
                                rightPadding: 4
                                selectByMouse: true
                                Component.onCompleted: Editor.setTextTarget("join", this, Profiles.getLaunch())
                            }

                            GaussianBlur {
                                id: joinBlur
                                radius: 8
                                samples: 10
                                deviation: 2
                                anchors.fill: joinInput
                                source: joinInput
                                visible: Profiles.hideJoinInput
                            }
                        }

                        Switch {
                            id: joinInputHide
                            height: 27
                            Layout.preferredWidth: 74
                            checked: Profiles.hideJoinInput
                        }
                    }

                    RowLayout {
                        id: netModeLayout
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: netModeLabel
                            color: "#ffffff"
                            text: qsTr("Network Mode")
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        ComboBox {
                            id: netModeInput
                            height: 27
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            bottomPadding: -10
                            topPadding: -10
                            font.family: "Verdana"
                            font.pointSize: 12
                            currentIndex: 0
                            textRole: "name"
                            model: ListModel {
                                id: netModeItems
                            }
                            Component.onCompleted: Editor.setNetModeTarget("netMode", this, Profiles.getLaunch())
                        }
                    }

                    RowLayout {
                        id: gameTypeLayout
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: gameTypeLabel
                            color: "#ffffff"
                            text: qsTr("Game Type")
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        ComboBox {
                            id: gameTypeInput
                            height: 27
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            bottomPadding: -10
                            topPadding: -10
                            font.family: "Verdana"
                            font.pointSize: 12
                            currentIndex: 0
                            textRole: "name"
                            model: ListModel {
                                id: gameTypeItems
                            }
                            Component.onCompleted: Editor.setGameTypeTarget("gameType", this, Profiles.getLaunch())
                        }
                    }

                    RowLayout {
                        id: playersLayout
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: playersLabel
                            color: "#ffffff"
                            text: qsTr("Players")
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Slider {
                            id: playerSlider
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204 - 46 - 8
                            height: 27
                            font.pointSize: 16
                            font.family: Arachnotron.mainFont
                            enabled: true
                            snapMode: Slider.SnapAlways
                            stepSize: 1
                            to: 32
                            from: 2
                            value: Profiles.getLaunch() ? Profiles.getLaunch().getDouble("players") : 2
                        }

                        TextField {
                            id: playersInput
                            Layout.preferredWidth: 46
                            height: 27
                            text: "2"
                            padding: -1
                            leftPadding: 8
                            topPadding: 3
                            bottomPadding: 0
                            font.family: "Verdana"
                            font.pointSize: 12
                            rightPadding: 4
                            selectByMouse: true
                            inputMethodHints: Qt.ImhDigitsOnly
                            validator: RegExpValidator{regExp: /\d+/}
                            Component.onCompleted: Editor.setDoubleTarget("players", this, Profiles.getLaunch())
                        }
                    }

                    RowLayout {
                        id: rowLayout
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: dupLabel
                            color: "#ffffff"
                            text: qsTr("Divide User Packets")
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Slider {
                            id: dupSlider
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204 - 46 - 8
                            height: 27
                            stepSize: 1
                            font.pointSize: 16
                            from: 1
                            snapMode: Slider.SnapAlways
                            value: Profiles.getLaunch() ? Profiles.getLaunch().getDouble("dup") : 1
                            font.family: Arachnotron.mainFont
                            to: 9
                            enabled: true
                        }

                        TextField {
                            id: dupInput
                            Layout.preferredWidth: 46
                            height: 27
                            text: "1"
                            padding: -1
                            leftPadding: 8
                            topPadding: 3
                            bottomPadding: 0
                            font.family: "Verdana"
                            font.pointSize: 12
                            rightPadding: 4
                            selectByMouse: true
                            inputMethodHints: Qt.ImhDigitsOnly
                            validator: RegExpValidator { regExp: /\d+/ }
                            Component.onCompleted: Editor.setDoubleTarget("dup", this, Profiles.getLaunch())
                        }
                    }

                    RowLayout {
                        id: rowLayout1
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: extraTicLabel
                            color: "#ffffff"
                            text: qsTr("Extra Tics")
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Slider {
                            id: extraTicSlider
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204 - 46 - 8
                            height: 27
                            font.pointSize: 16
                            stepSize: 1
                            from: 0
                            snapMode: Slider.SnapAlways
                            value: Profiles.getLaunch() ? Profiles.getLaunch().getDouble("extraTics") : 0
                            font.family: Arachnotron.mainFont
                            to: 2
                            enabled: true
                        }

                        TextField {
                            id: extraTicInput
                            Layout.preferredWidth: 46
                            height: 27
                            text: "0"
                            padding: -1
                            leftPadding: 8
                            topPadding: 3
                            bottomPadding: 0
                            font.family: "Verdana"
                            font.pointSize: 12
                            rightPadding: 4
                            selectByMouse: true
                            inputMethodHints: Qt.ImhDigitsOnly
                            validator: RegExpValidator { regExp: /\d+/ }
                            Component.onCompleted: Editor.setDoubleTarget("extraTics", this, Profiles.getLaunch())
                        }
                    }

                    RowLayout {
                        id: rowLayout2
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: ticBalanceLabel
                            color: "#ffffff"
                            text: qsTr("Tic Balancing")
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Switch {
                            id: ticBalanceInput
                            height: 27
                            checked: false
                            Component.onCompleted: Editor.setBoolTarget("ticBalance", this, Profiles.getLaunch())
                        }
                    }

                    RowLayout {
                        id: rowLayout3
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: cheatsLabel
                            color: "#ffffff"
                            text: qsTr("Cheats")
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Switch {
                            id: cheatsInput
                            height: 27
                            checked: false
                            Component.onCompleted: Editor.setBoolTarget("cheats", this, Profiles.getLaunch())
                        }
                    }

                    RowLayout {
                        spacing: 8
                        width: parent.width
                        opacity: 0.3
                        layer.enabled: true
                        Layout.fillWidth: true

                        Text {
                            id: deathmatchWeaponsLabel
                            color: "#ffffff"
                            text: qsTr("Deathmatch Weapons")
                            Layout.fillWidth: true
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Switch {
                            id: deathmatchWeaponsInput
                            height: 27
                            checked: false
                            Component.onCompleted: Editor.setBoolTarget("deathmatchWeapons", this, Profiles.getLaunch())
                        }
                    }

                    ColumnLayout {
                        width: parent.width
                        spacing: 8
                        Layout.fillWidth: true

                        RowLayout {
                            id: row
                            spacing: 8
                            width: parent.width
                            height: 32
                            Layout.fillWidth: true

                            Text {
                                id: launchCommandLabel
                                width: 240
                                color: "#ffffff"
                                text: qsTr("Launch Command Preview")
                                Layout.fillWidth: true
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                font.family: Arachnotron.mainFont
                                lineHeight: 1.22
                            }

                            Button {
                                id: launchCommandButton
                                width: 120
                                height: 32
                                text: "Generate"
                                padding: 4
                                font.bold: true
                                font.family: Arachnotron.mainFont
                                font.pointSize: 14
                                icon.source: "../assets/engineIcon.svg"
                                highlighted: launchMouseCatch.containsMouse

                                MouseArea {
                                    id: launchMouseCatch
                                    anchors.fill: parent
                                    hoverEnabled: true
                                }
                            }
                        }

                        TextArea {
                            id: launchCommandOutput
                            width: parent.width
                            height: 128
                            text: "Click Generate to show the launch command here. Note that clicking Generate will save your default launch settings."
                            font.family: "Verdana"
                            wrapMode: Text.WordWrap
                            Layout.minimumHeight: 334
                            leftPadding: 4
                            Layout.fillWidth: true
                            font.pointSize: 10
                            padding: 4
                            selectByMouse: true
                            readOnly: true
                            background: Rectangle {
                                color: "#eeeeee"
                            }
                        }
                    }
                }
            }
        }
    }

    Connections {
        target: loadMouseCatch
        onClicked: FileLoader.openFileDialog("Load Game Location", Editor.setPath, loadGameInput, loadGameInput.text)
    }

    Connections {
        target: launchMouseCatch
        onClicked: Profiles.loadLaunchCommandPreview(launchCommandOutput);
    }

    Connections {
        target: playerClassInput
        onEditTextChanged: Editor.updateEditableCombo(playerClassInput)
    }

    Connections {
        target: joinInputHide
        onToggled: {
            Profiles.hideJoinInput = joinInputHide.checked;
            joinBlur.visible = Profiles.hideJoinInput;
        }
    }

    Connections {
        target: playerSlider
        onMoved: playersInput.text = playerSlider.value
    }

    Connections {
        target: playersInput
        onTextEdited: playerSlider.value = parseInt(playersInput.text)
    }

    Connections {
        target: dupSlider
        onMoved: dupInput.text = dupSlider.value
    }

    Connections {
        target: dupInput
        onTextEdited: dupSlider.value = parseInt(dupInput.text)
    }

    Connections {
        target: extraTicSlider
        onMoved: extraTicInput.text = extraTicSlider.value
    }

    Connections {
        target: extraTicInput
        onTextEdited: extraTicSlider.value = parseInt(extraTicInput.text)
    }

    states: [
        State {
            name: "join"
            when: multiplayerEnabledInput.checked && !hostInput.checked

            PropertyChanges {
                target: joinLayout
                opacity: 1
            }

            PropertyChanges {
                target: hostLayout
                opacity: 1
            }
        },
        State {
            name: "host"
            when: multiplayerEnabledInput.checked && hostInput.checked

            PropertyChanges {
                target: hostLayout
                opacity: 1
            }

            PropertyChanges {
                target: netModeLayout
                opacity: 1
            }

            PropertyChanges {
                target: gameTypeLayout
                opacity: 1
            }

            PropertyChanges {
                target: playersLayout
                opacity: 1
            }

            PropertyChanges {
                target: rowLayout
                opacity: 1
            }

            PropertyChanges {
                target: rowLayout1
                opacity: 1
            }

            PropertyChanges {
                target: rowLayout2
                opacity: 1
            }

            PropertyChanges {
                target: rowLayout3
                opacity: 1
            }

            PropertyChanges {
                target: rowLayout4
                opacity: 1
            }
        }
    ]

    transitions: [
        Transition {
            PropertyAnimation {
                properties: "opacity"
                easing.type: Easing.InOutQuad
                duration: 250
            }
        }
    ]
}
