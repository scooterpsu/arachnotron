import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/profiles.js" as Profiles

Item {
    id: profileButton
    width: 272
    height: 200

    property int profileId: -1;
    property var name: nameLabel;
    property var icon: iconImage;
    property var background: backgroundImage;
    property var backgroundDefault: "../assets/ControlsBG.png";
    property var selectedEffect: glowEffect;

    Glow {
        id: glowEffect
        anchors.fill: buttonArea
        radius: 3
        samples: 17
        spread: 0.25
        color: "#ffffff"
        source: buttonArea
        visible: buttonArea.containsMouse
    }

    MouseArea {
        id: buttonArea
        anchors.fill: parent
        hoverEnabled: true

        Image {
            id: backgroundImage
            height: 170
            anchors.top: parent ? parent.top : undefined
            anchors.right: parent ? parent.right : undefined
            anchors.left: parent ? parent.left : undefined
            source: backgroundDefault
            fillMode: Image.Tile

            Image {
                id: iconImage
                anchors.fill: parent
                source: "../assets/profile.png"
                fillMode: Image.PreserveAspectFit

                Row {
                    id: buttons
                    visible: buttonArea.containsMouse
                    spacing: 2
                    layoutDirection: Qt.RightToLeft
                    anchors.bottom: parent ? parent.bottom : undefined
                    anchors.bottomMargin: 0
                    anchors.right: parent ? parent.right : undefined
                    anchors.rightMargin: 0
                    anchors.left: parent ? parent.left : undefined
                    anchors.leftMargin: 0

                    Button {
                        id: launchButton
                        width: 32
                        height: 32
                        highlighted: !launchMouseCatch.containsMouse
                        icon.source: "../assets/launchIcon.svg"

                        MouseArea {
                            id: launchMouseCatch
                            anchors.fill: parent
                            hoverEnabled: true
                        }
                    }

                    Button {
                        id: editButton
                        width: 32
                        height: 32
                        highlighted: !editMouseCatch.containsMouse
                        icon.source: "../assets/editIcon.svg"

                        MouseArea {
                            id: editMouseCatch
                            anchors.fill: parent
                            hoverEnabled: true
                        }
                    }
                }
            }
        }

        Text {
            id: nameLabel
            height: 24
            color: "#ffffff"
            text: qsTr("The Ultimate Doom")
            font.capitalization: Font.SmallCaps
            font.bold: true
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.right: parent ? parent.right : undefined
            anchors.left: parent ? parent.left : undefined
            anchors.topMargin: 5
            anchors.top: backgroundImage.bottom
        }
    }

    Connections {
        target: buttonArea
        onClicked: Profiles.quickLaunch(profileButton)
    }

    Connections {
        target: launchMouseCatch
        onClicked: Profiles.selectProfile(profileButton)
    }

    Connections {
        target: editMouseCatch
        onClicked: Profiles.quickEdit(profileButton)
    }
}
