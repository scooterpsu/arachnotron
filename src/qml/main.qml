import QtQuick 2.9
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.0
import Arachnotron 1.0
import "../scripts/main.js" as Main
import "../scripts/workspace.js" as Workspace
import "../scripts/controls.js" as Controls
import "../scripts/popupWindow.js" as PopupWindow
import "../scripts/dialogWindow.js" as DialogWindow
import "../scripts/settings.js" as Settings
import "../scripts/categories.js" as Categories
import "../scripts/profiles.js" as Profiles
import "../scripts/fileLoader.js" as FileLoader

Window {
    id: mainWindow
    title: settingsManager.getAppName() + " " + settingsManager.getAppVersion()
    //flags: Qt.FramelessWindowHint
    visible: true
    width: 1250
    height: 715
    minimumWidth: 720
    minimumHeight: 320
    color: "#2e0038"
    opacity: 1

    Settings {
        id: settings
        property alias x: mainWindow.x
        property alias y: mainWindow.y
        property var desktopAvailableWidth
        property var desktopAvailableHeight
        property alias width: mainWindow.width
        property alias height: mainWindow.height
    }

    function saveScreenLayout() {
        settings.desktopAvailableWidth = Screen.desktopAvailableWidth
        settings.desktopAvailableHeight = Screen.desktopAvailableHeight
    }

    function ensureValidWindowPosition() {
        var savedScreenLayout = (settings.desktopAvailableWidth === Screen.desktopAvailableWidth)
                && (settings.desktopAvailableHeight === Screen.desktopAvailableHeight)
        mainWindow.x = (savedScreenLayout) ? settings.x : Screen.width / 2 - mainWindow.width / 2
        mainWindow.y = (savedScreenLayout) ? settings.y : Screen.height / 2 - mainWindow.height / 2
    }

    Component.onCompleted: {
        FileLoader.setSettingsManager(settingsManager);
        Settings.setSettingsManager(settingsManager);
        Categories.setCategoryManager(categoryManager);
        Profiles.setProfileManager(profileManager);
        ensureValidWindowPosition();
    }

    Component.onDestruction: saveScreenLayout();

    Rectangle {
        id: content
        color: "#00000000"
        anchors.fill: parent
        border.color: "#00000000"

        Image {
            id: categoriesBackground
            width: 96
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
            source: "../assets/CategoryListBG.png"
            fillMode: Image.Tile

            Image {
                id: categorySelectionBorder
                x: 4
                y: 0
                width: 32
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.top: parent.top
                anchors.bottomMargin: 0
                anchors.bottom: parent.bottom
                anchors.topMargin: 0
                source: "../assets/CategoryListBorder.png"
                fillMode: Image.Tile
            }

            ScrollView {
                id: categoriesView
                topPadding: 16
                anchors.bottomMargin: 32
                anchors.rightMargin: 22
                rightPadding: 10
                anchors.fill: parent

                Column {
                    id: categoriesLayout
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.top: parent.top
                    anchors.topMargin: 0

                    Column {
                        id: categoryList
                        anchors.right: parent.right
                        anchors.rightMargin: 0
                        anchors.left: parent.left
                        anchors.leftMargin: 0
                        spacing: 8

                        Component.onCompleted: Categories.setCategoryListTarget(this)
                    }

                    MouseArea {
                        id: addCategoryButton
                        width: 64
                        height: 64
                        hoverEnabled: true

                        Image {
                            id: addCategoryImage
                            anchors.rightMargin: 16
                            anchors.leftMargin: 16
                            anchors.bottomMargin: 16
                            anchors.topMargin: 16
                            anchors.fill: parent
                            source: "../assets/addIcon.svg"
                            fillMode: Image.PreserveAspectFit
                        }

                        Colorize {
                            id: addCategoryImageColor
                            anchors.fill: addCategoryImage
                            source: addCategoryImage
                            hue: 0.0
                            saturation: 1
                            lightness: 1
                        }

                        Glow {
                            id: glowEffect
                            anchors.fill: addCategoryImageColor
                            radius: 3
                            samples: 17
                            spread: 0.45
                            color: "#ffffff"
                            source: addCategoryImageColor
                            visible: addCategoryButton.containsMouse
                        }
                    }
                }
            }
        }

        Rectangle {
            id: workspaceRect
            x: 64
            y: 0
            color: "#00000000"
            anchors.left: categoriesBackground.right
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            border.color: "#00000000"


            Image {
                id: workspaceBG
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                source: "../assets/WorkspaceBG.png"
                fillMode: Image.Tile
            }

            DropShadow {
                id: workspaceShadow
                x: 0
                y: 0
                color: "#000000"
                anchors.fill: workspace
                source: workspace
                horizontalOffset: 2
                verticalOffset: 2
                radius: 8.0
                transparentBorder: true
                samples: 17
            }

            Rectangle {
                id: workspace
                color: "#00000000"
                border.color: "#00000000"
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                anchors.bottomMargin: 10
                anchors.topMargin: 0

                Component.onCompleted: Workspace.setWorkspaceTarget(this)
            }
        }

        Image {
            id: controls
            x: 0
            y: 528
            height: 32
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            source: "../assets/ControlsBG.png"
            fillMode: Image.Tile

            Component.onCompleted: Controls.setControlsTarget(this)

            Row {
                id: settingsRow
                layoutDirection: Qt.LeftToRight
                anchors.fill: parent
                spacing: 4

                Button {
                    id: settingsButton
                    width: 32
                    height: 32
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                    font.family: Arachnotron.mainFont
                    highlighted: !settingsMouseCatch.containsMouse
                    font.capitalization: Font.MixedCase
                    font.pointSize: 22
                    checked: false
                    flat: false
                    autoRepeat: false
                    checkable: false
                    autoExclusive: false
                    wheelEnabled: false
                    icon.source: "../assets/settingsIcon.svg"

                    ToolTip {
                        id: settingsToolTip
                        text: "Settings"
                        visible: settingsMouseCatch.containsMouse
                    }

                    MouseArea {
                        id: settingsMouseCatch
                        anchors.fill: parent
                        hoverEnabled: true
                    }
                }

                Button {
                    id: enginesButton
                    x: 0
                    width: 32
                    height: 32
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                    autoRepeat: false
                    font.capitalization: Font.MixedCase
                    checked: false
                    wheelEnabled: false
                    flat: false
                    autoExclusive: false
                    checkable: false
                    highlighted: !enginesMouseCatch.containsMouse
                    font.family: Arachnotron.mainFont
                    font.pointSize: 22
                    icon.source: "../assets/engineIcon.svg"

                    ToolTip {
                        id: enginesToolTip
                        text: "Engines"
                        visible: enginesMouseCatch.containsMouse
                    }

                    MouseArea {
                        id: enginesMouseCatch
                        anchors.fill: parent
                        hoverEnabled: true
                    }
                }

                Button {
                    id: iwadsButton
                    width: 32
                    height: 32
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                    autoRepeat: false
                    font.capitalization: Font.MixedCase
                    checked: false
                    wheelEnabled: false
                    flat: false
                    autoExclusive: false
                    checkable: false
                    highlighted: !iwadsMouseCatch.containsMouse
                    font.family: Arachnotron.mainFont
                    font.pointSize: 22
                    icon.source: "../assets/fileIcon.svg"

                    ToolTip {
                        id: iwadsToolTip
                        text: "IWADS"
                        visible: iwadsMouseCatch.containsMouse
                    }

                    MouseArea {
                        id: iwadsMouseCatch
                        anchors.fill: parent
                        hoverEnabled: true
                    }
                }

                Button {
                    id: importButton
                    width: 32
                    height: 32
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                    autoRepeat: false
                    font.capitalization: Font.MixedCase
                    checked: false
                    wheelEnabled: false
                    flat: false
                    autoExclusive: false
                    checkable: false
                    highlighted: !importMouseCatch.containsMouse
                    font.family: Arachnotron.mainFont
                    font.pointSize: 22
                    icon.source: "../assets/importIcon.svg"

                    ToolTip {
                        id: importToolTip
                        text: "Import"
                        visible: importMouseCatch.containsMouse
                    }

                    MouseArea {
                        id: importMouseCatch
                        anchors.fill: parent
                        hoverEnabled: true
                    }
                }
            }
        }

    }


    GaussianBlur {
        id: popupBlur
        x: 9
        y: 6
        radius: 8
        anchors.fill: content
        samples: 16
        source: content
        visible: false
        Component.onCompleted: PopupWindow.setPopupBlur(this)

        MouseArea {
            id: popupBlurMouseCatch
            anchors.fill: parent
        }
    }

    Rectangle {
        id: popup
        color: "#00000000"
        border.color: "#00000000"
        anchors.fill: parent
        Component.onCompleted: PopupWindow.setPopupTarget(this)
    }

    GaussianBlur {
        id: dialogBlur
        x: 12
        y: 9
        radius: 8
        samples: 16
        visible: false
        anchors.fill: content
        source: content
        Component.onCompleted: DialogWindow.setDialogBlur(this)

        MouseArea {
            id: dialogBlurMouseCatch
            anchors.fill: parent
        }
    }

    Rectangle {
        id: dialog
        x: 3
        y: 3
        color: "#00000000"
        border.color: "#00000000"
        anchors.fill: parent
        Component.onCompleted: DialogWindow.setDialogTarget(this)
    }

    FileDialog {
        id: fileDialog
        onAccepted: FileLoader.fileDialogAccepted();
        Component.onCompleted: FileLoader.setFileDialog(this)
    }

    Connections {
        target: settingsMouseCatch
        onClicked: Main.openSettings()
    }

    Connections {
        target: enginesMouseCatch
        onClicked: Main.openEngines()
    }

    Connections {
        target: iwadsMouseCatch
        onClicked: Main.openIwads()
    }

    Connections {
        target: importMouseCatch
        onClicked: Settings.importFile()
    }

    Connections {
        target: addCategoryButton
        onClicked: Categories.createCategory()
    }
}
