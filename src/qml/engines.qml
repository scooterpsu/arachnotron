import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import Arachnotron 1.0
import "../scripts/popupWindow.js" as PopupWindow
import "../scripts/engines.js" as Engines

Item {
    id: engines
    visible: true
    width: 1024
    height: 560

    Image {
        id: popupWindow
        anchors.rightMargin: 60
        anchors.leftMargin: 60
        anchors.bottomMargin: 60
        anchors.topMargin: 60
        anchors.fill: parent
        fillMode: Image.Tile
        source: "../assets/EnginesBG.png"


        DropShadow {
            id: contentShadow
            x: 0
            y: 0
            color: "#000000"
            radius: 8
            anchors.fill: contentRect
            samples: 17
            horizontalOffset: 2
            transparentBorder: true
            verticalOffset: 2
            source: contentRect
        }

        Rectangle {
            id: contentRect
            color: "#00000000"
            border.color: "#00000000"
            anchors.fill: parent

            Text {
                id: popupTitle
                x: 0
                y: 8
                color: "#ffffff"
                text: "Engines"
                anchors.top: parent.top
                anchors.topMargin: 8
                font.pointSize: 16
                font.family: Arachnotron.bigFont
                styleColor: "#00000000"
                horizontalAlignment: Text.AlignHCenter
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
            }

            ColumnLayout {
                id: enginesLayout
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 56
                anchors.top: popupTitle.bottom
                anchors.topMargin: 0
                anchors.rightMargin: 16
                anchors.leftMargin: 16
                anchors.right: parent.right
                anchors.left: parent.left
                Layout.fillWidth: true
                spacing: 8


                RowLayout {
                    id: headersLayout
                    Layout.fillWidth: true

                    Text {
                        id: namesLabel
                        color: "#ffffff"
                        text: qsTr("Name")
                        font.family: Arachnotron.mainFont
                        font.pointSize: 16
                        Layout.fillWidth: true
                        verticalAlignment: Text.AlignBottom
                        horizontalAlignment: Text.AlignHCenter
                        lineHeight: 1
                        leftPadding: 70
                        topPadding: 8
                    }

                    Text {
                        id: pathsLabel
                        color: "#ffffff"
                        text: qsTr("Executable Path")
                        lineHeight: 1
                        font.pointSize: 16
                        verticalAlignment: Text.AlignBottom
                        font.family: Arachnotron.mainFont
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                        leftPadding: 80
                        topPadding: 8
                    }

                    Text {
                        id: configsLabel
                        color: "#ffffff"
                        text: qsTr("Config Path (Optional)")
                        lineHeight: 1
                        font.pointSize: 16
                        verticalAlignment: Text.AlignBottom
                        font.family: Arachnotron.mainFont
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                        rightPadding: 80
                        topPadding: 1
                    }
                }
                Rectangle {
                    id: enginesBackground
                    color: "#40000000"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    border.color: "#00000000"

                    Component.onCompleted: Engines.loadEnginesList(this)
                }
            }

            Button {
                id: closeButton
                x: 804
                y: 400
                text: qsTr("Close")
                font.family: Arachnotron.bigFont
                highlighted: !closeMouseCatch.containsMouse
                font.bold: false
                font.pointSize: 12
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0

                MouseArea {
                    id: closeMouseCatch
                    anchors.fill: parent
                    hoverEnabled: true
                }
            }
        }
    }

    Connections {
        target: closeMouseCatch
        onClicked: {
            Engines.saveSettings();
            PopupWindow.setPopupForm("");
        }
    }
}

















































































































































































































/*##^## Designer {
    D{i:2;anchors_x:0;anchors_y:0}D{i:5;anchors_height:200;anchors_width:200}D{i:3;anchors_height:200;anchors_width:200}
D{i:1;anchors_height:100;anchors_width:100}
}
 ##^##*/
