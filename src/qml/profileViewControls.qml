import QtQuick 2.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/profiles.js" as Profiles

Item {
    width: 1024
    height: 32

    Row {
        id: launchRow
        anchors.fill: parent ? parent : undefined
        layoutDirection: Qt.RightToLeft
        spacing: 4

        Button {
            id: launchButton
            height: 32
            text: qsTr("Launch")
            rightPadding: 16
            leftPadding: 10
            anchors.bottom: parent ? parent.bottom : undefined
            anchors.bottomMargin: 0
            highlighted: !launchMouseCatch.containsMouse
            font.capitalization: Font.MixedCase
            font.pointSize: 12
            font.family: Arachnotron.bigFont
            icon.source: "../assets/launchIcon.svg"

            MouseArea {
                id: launchMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: editButton
            height: 32
            width: 32
            anchors.bottom: parent ? parent.bottom : undefined
            anchors.bottomMargin: 0
            font.capitalization: Font.MixedCase
            wheelEnabled: false
            highlighted: !editMouseCatch.containsMouse
            font.family: Arachnotron.bigFont
            font.pointSize: 12
            icon.source: "../assets/editIcon.svg"

            ToolTip {
                id: editToolTip
                text: "Edit Profile"
                visible: editMouseCatch.containsMouse
            }

            MouseArea {
                id: editMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: saveButton
            height: 32
            width: 32
            highlighted: !saveMouseCatch.containsMouse
            icon.source: "../assets/importIcon.svg"
            anchors.bottom: parent ? parent.bottom : undefined
            wheelEnabled: false
            font.pointSize: 12
            anchors.bottomMargin: 0
            font.family: Arachnotron.bigFont
            font.capitalization: Font.MixedCase

            ToolTip {
                id: saveToolTip
                text: "Save Defaults"
                visible: saveMouseCatch.containsMouse
            }

            MouseArea {
                id: saveMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: refreshButton
            height: 32
            width: 32
            highlighted: !refreshMouseCatch.containsMouse
            icon.source: "../assets/refreshIcon.svg"
            anchors.bottom: parent ? parent.bottom : undefined
            wheelEnabled: false
            font.pointSize: 12
            anchors.bottomMargin: 0
            font.family: Arachnotron.bigFont
            font.capitalization: Font.MixedCase

            ToolTip {
                id: refreshToolTip
                text: "Reload"
                visible: refreshMouseCatch.containsMouse
            }

            MouseArea {
                id: refreshMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

    }

    Connections {
        target: launchMouseCatch
        onClicked: Profiles.launch()
    }

    Connections {
        target: editMouseCatch
        onClicked: Profiles.editProfile()
    }

    Connections {
        target: saveMouseCatch
        onClicked: Profiles.saveLaunch()
    }

    Connections {
        target: refreshMouseCatch
        onClicked: Profiles.selectProfile()
    }
}
