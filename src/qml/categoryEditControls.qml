import QtQuick 2.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/categories.js" as Categories
import "../scripts/fileLoader.js" as FileLoader

Item {
    width: 1024
    height: 32
    Row {
        id: editRow
        anchors.fill: parent ? parent : undefined
        layoutDirection: Qt.RightToLeft
        spacing: 4

        Button {
            id: saveButton
            height: 32
            text: qsTr("Save")
            rightPadding: 16
            leftPadding: 10
            font.capitalization: Font.MixedCase
            anchors.bottom: parent ? parent.bottom : undefined
            anchors.bottomMargin: 0
            highlighted: !saveMouseCatch.containsMouse
            font.bold: true
            font.family: Arachnotron.bigFont
            font.pointSize: 12
            icon.source: "../assets/saveIcon.svg"

            MouseArea {
                id: saveMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: resetButton
            height: 32
            width: 32
            highlighted: !resetMouseCatch.containsMouse
            icon.source: "../assets/refreshIcon.svg"
            anchors.bottom: parent ? parent.bottom : undefined
            wheelEnabled: false
            font.pointSize: 12
            anchors.bottomMargin: 0
            font.family: Arachnotron.bigFont
            font.capitalization: Font.MixedCase

            ToolTip {
                id: resetToolTip
                text: "Reload"
                visible: resetMouseCatch.containsMouse
            }

            MouseArea {
                id: resetMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: removeButton
            height: 32
            width: 32
            highlighted: !removeMouseCatch.containsMouse
            icon.source: "../assets/removeIcon.svg"
            font.pointSize: 12
            anchors.bottomMargin: 0
            font.family: Arachnotron.bigFont

            ToolTip {
                id: removeToolTip
                text: "Remove"
                visible: removeMouseCatch.containsMouse
            }

            MouseArea {
                id: removeMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }
    }

    Connections {
        target: saveMouseCatch
        onClicked: Categories.saveCategory()
    }

    Connections {
        target: resetMouseCatch
        onClicked: Categories.editCategory()
    }

    Connections {
        target: removeMouseCatch
        onClicked: Categories.removeCategory()
    }
}
