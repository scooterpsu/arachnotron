import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Arachnotron 1.0
import "../scripts/fileLoader.js" as FileLoader
import "../scripts/listEntry.js" as ListEntry

Item {
    id: fileEntry
    width: 832
    height: 32

    property var inputFields: [nameInput, pathInput, configInput]
    property var rootDirectoryInput: null
    property var list: null
    property var load: ListEntry.loadEngineEntry;
    property var remove: ListEntry.removeEntry;

    RowLayout {
        id: fileLayout
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 10
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 10
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.bottomMargin: 1
        anchors.top: parent ? parent.top : undefined
        anchors.topMargin: 1

        TextField {
            id: nameInput
            text: qsTr("Name")
            font.family: "Verdana"
            font.pointSize: 10
            selectByMouse: true
            leftPadding: 8
            topPadding: 1
            bottomPadding: 0
            rightPadding: 4
            Layout.fillWidth: true
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
        }

        TextField {
            id: pathInput
            text: qsTr("")
            font.family: "Verdana"
            font.pointSize: 10
            selectByMouse: true
            leftPadding: 8
            topPadding: 1
            bottomPadding: 0
            rightPadding: 4
            Layout.fillWidth: true
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27

            Component.onCompleted: ListEntry.setPathTarget(this)
        }

        Button {
            id: fileButton
            font.pointSize: 16
            font.family: Arachnotron.mainFont
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.preferredWidth: 27
            Layout.minimumWidth: 27
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumWidth: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/fileIcon.svg"
            highlighted: fileMouseCatch.containsMouse

            MouseArea {
                id: fileMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        TextField {
            id: configInput
            text: qsTr("")
            font.family: "Verdana"
            font.pointSize: 10
            selectByMouse: true
            leftPadding: 8
            topPadding: 1
            bottomPadding: 0
            rightPadding: 4
            Layout.fillWidth: true
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27

            Component.onCompleted: ListEntry.setConfigTarget(this)
        }

        Button {
            id: configFileButton
            font.pointSize: 16
            font.family: Arachnotron.mainFont
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.preferredWidth: 27
            Layout.minimumWidth: 27
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumWidth: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/fileIcon.svg"
            highlighted: configMouseCatch.containsMouse

            MouseArea {
                id: configMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: upButton
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/upArrowIcon.svg"
            highlighted: upMouseCatch.containsMouse

            MouseArea {
                id: upMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: downButton
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/downArrowIcon.svg"
            highlighted: downMouseCatch.containsMouse

            MouseArea {
                id: downMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: removeButton
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/removeIcon.svg"
            highlighted: removeMouseCatch.containsMouse

            MouseArea {
                id: removeMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }
    }

    Connections {
        target: removeMouseCatch
        onClicked: {
            list.removeEntry(fileEntry)
            ListEntry.removeEntry(fileEntry)
        }
    }

    Connections {
        target: upMouseCatch
        onClicked: list.moveEntryUp(fileEntry)
    }

    Connections {
        target: downMouseCatch
        onClicked: list.moveEntryDown(fileEntry)
    }

    Connections {
        target: fileMouseCatch
        onClicked: FileLoader.openFileDialog("Engine Executable Location", ListEntry.setPath, pathInput, configInput.text)
    }

    Connections {
        target: configMouseCatch
        onClicked: FileLoader.openFileDialog("Engine Config Location", ListEntry.setPath, configInput, configInput.text)
    }
}
