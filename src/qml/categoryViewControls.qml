import QtQuick 2.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/categories.js" as Categories
import "../scripts/profiles.js" as Profiles

Item {
    width: 1024
    height: 32

    Row {
        id: launchRow
        anchors.fill: parent ? parent : undefined
        layoutDirection: Qt.RightToLeft
        spacing: 4

        Button {
            id: launchButton
            height: 32
            text: qsTr("Add Profile")
            rightPadding: 16
            leftPadding: 12
            anchors.bottomMargin: 0
            highlighted: !launchMouseCatch.containsMouse
            font.capitalization: Font.MixedCase
            font.pointSize: 12
            font.family: Arachnotron.bigFont
            icon.source: "../assets/addIcon.svg"

            MouseArea {
                id: launchMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: editButton
            width: 32
            height: 32
            anchors.bottomMargin: 0
            highlighted: !editMouseCatch.containsMouse
            font.family: Arachnotron.bigFont
            font.pointSize: 12
            icon.source: "../assets/editIcon.svg"
            visible: Categories.selectedCategory.getString("name") !== "All"

            ToolTip {
                id: editToolTip
                text: "Edit Category"
                visible: editMouseCatch.containsMouse
            }

            MouseArea {
                id: editMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: refreshButton
            width: 32
            height: 32
            anchors.bottomMargin: 0
            highlighted: !refreshMouseCatch.containsMouse
            font.family: Arachnotron.bigFont
            font.pointSize: 12
            icon.source: "../assets/refreshIcon.svg"

            ToolTip {
                id: refreshToolTip
                text: "Reload"
                visible: refreshMouseCatch.containsMouse
            }

            MouseArea {
                id: refreshMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }
    }

    Connections {
        target: launchMouseCatch
        onClicked: Profiles.addProfile()
    }

    Connections {
        target: editMouseCatch
        onClicked: Categories.editCategory()
    }

    Connections {
        target: refreshMouseCatch
        onClicked: Categories.selectCategory(Categories.selectedCategory)
    }
}
