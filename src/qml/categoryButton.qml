import QtQuick 2.0
import QtGraphicalEffects 1.0
import Arachnotron 1.0
import "../scripts/categories.js" as Categories

Item {
    id: categoryButton
    width: 64
    height: 88

    property int categoryId: -1;
    property var name: nameLabel;
    property var icon: iconImage;
    property var selectedEffect: glowEffect;

    Component.onCompleted: {
        Categories.selectCategory(categoryButton.categoryId);
    }

    Glow {
        id: glowEffect
        anchors.fill: buttonArea
        radius: 3
        samples: 17
        spread: 0.45
        color: "#ffffff"
        source: buttonArea
        visible: buttonArea.containsMouse
    }

    MouseArea {
        id: buttonArea
        anchors.fill: parent
        hoverEnabled: true

        Image {
            id: iconImage
            anchors.bottom: nameLabel.top
            anchors.right: parent ? parent.right : undefined
            anchors.left: parent ? parent.left : undefined
            anchors.top: parent ? parent.top : undefined
            fillMode: Image.PreserveAspectFit
            source: "../assets/category.png"
        }

        Text {
            id: nameLabel
            height: 24
            color: "#ffffff"
            text: qsTr("Doom")
            anchors.bottom: parent ? parent.bottom : undefined
            anchors.bottomMargin: 0
            font.capitalization: Font.SmallCaps
            font.bold: true
            font.family: Arachnotron.mainFont
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 16
            anchors.right: parent ? parent.right : undefined
            anchors.rightMargin: 0
            anchors.left: parent ? parent.left : undefined
            anchors.leftMargin: 0
        }
    }

    Connections {
        target: buttonArea
        onClicked: Categories.selectCategory(categoryButton)
    }
}
