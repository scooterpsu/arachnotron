import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Arachnotron 1.0
import "../scripts/fileLoader.js" as FileLoader
import "../scripts/listEntry.js" as ListEntry
import "../scripts/editor.js" as Editor

Item {
    id: listEntry
    width: 416
    height: 32

    property var inputFields: [valueInput]
    property var rootDirectoryInput: null
    property var list: null
    property var load: ListEntry.loadDropdownEntry
    property var remove: ListEntry.removeEntry;
    property var setComboTarget: Editor.setComboTarget;

    RowLayout {
        id: fileLayout
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 10
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 10
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.bottomMargin: 1
        anchors.top: parent ? parent.top : undefined
        anchors.topMargin: 1

        ComboBox {
            id: valueInput
            height: 27
            font.pointSize: 10
            Layout.fillWidth: true
            Layout.preferredWidth: 204
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            currentIndex: -1
            textRole: "name"
            model: ListModel {
                id: valueInputItems
            }
            font.family: Arachnotron.mainFont
            editable: true
            contentItem: TextField {
                text: valueInput.editText
                font.family: "Verdana"
                font.pointSize: 10
                selectByMouse: true
            }
        }

        Button {
            id: upButton
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/upArrowIcon.svg"
            highlighted: upMouseCatch.containsMouse

            MouseArea {
                id: upMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: downButton
            font.family: Arachnotron.mainFont
            font.pointSize: 12
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/downArrowIcon.svg"
            highlighted: downMouseCatch.containsMouse

            MouseArea {
                id: downMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }

        Button {
            id: removeButton
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            font.family: Arachnotron.mainFont
            font.pointSize: 12
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/removeIcon.svg"
            highlighted: removeMouseCatch.containsMouse

            MouseArea {
                id: removeMouseCatch
                anchors.fill: parent
                hoverEnabled: true
            }
        }
    }

    Connections {
        target: removeMouseCatch
        onClicked: {
            list.removeEntry(listEntry)
            ListEntry.removeEntry(listEntry)
        }
    }

    Connections {
        target: upMouseCatch
        onClicked: list.moveEntryUp(listEntry)
    }

    Connections {
        target: downMouseCatch
        onClicked: list.moveEntryDown(listEntry)
    }
}
