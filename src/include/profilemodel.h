#ifndef PROFILEMODEL_H
#define PROFILEMODEL_H

#include "include/profilelaunch.h"

class ProfileModel : public ProfileBase {
    Q_OBJECT

protected:
    ProfileLaunch * launchSettings = nullptr;

public:
    // Constructor
    ProfileModel();

public slots:
    virtual QString getModelType() override;
    QList<ProfileModel *> getInheritedProfiles() const;

    // JSON
    virtual bool readFromJson() override;
    virtual bool writeToJson() override;

    // Getters
    QString getInheritedString(QString) const;
    QVector<QString> getInheritedList(QString) const;
    QHash<QString, QString> getInheritedMap(QString) const;
    QString getInheritedMapValue(QString, QString) const;
    QList<QString> getInheritedMapKeys(QString) const;

    // Launch
    bool launch();
    QString getLaunchCommand();
    ProfileLaunch * getLaunchSettings();

    // Categories
    bool isInCategory(QString categoryName);
};

#endif
