#ifndef PROFILEBASE_H
#define PROFILEBASE_H

#include "include/jsonloader.h"

#include <QTextStream>

class ProfileBase : public JsonLoader {
    Q_OBJECT

public:
    // Constructor
    ProfileBase();

public slots:
    // Launch
    void addArg(QTextStream &, QString);
    void addArg(QTextStream &, QString, QString);
    void addArgQuotes(QTextStream &, QString, QString);
    void addArg(QTextStream &, QString, bool);
    void addArg(QTextStream &, QString, int);
    void addArg(QTextStream &, QString, double);
    void addArg(QTextStream &, QString, double, double);
};

#endif // PROFILEBASE_H
