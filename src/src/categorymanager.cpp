#include "include/categorymanager.h"

#include <QDir>
#include <QDebug>

CategoryManager * CategoryManager::instance = nullptr;

// Constructor
CategoryManager::CategoryManager(QQmlEngine * engine)
{
    this->qmlEngine = engine;
    engine->setObjectOwnership(this, QQmlEngine::CppOwnership);
    this->setSourcePath("config/categories.json");
}

// Initialize
bool CategoryManager::init()
{
    CategoryManager::instance = this;

    // All Profiles Category:
    CategoryModel * allCategory = new CategoryModel();
    allCategory->internal = false;
    allCategory->allProfiles = true;
    allCategory->setSourcePath("assets");
    allCategory->setString("name", "All");
    allCategory->setString("iconPath", "config/categories/all.png");
    this->addCategory(allCategory);

    return this->readFromJson();
}

// JSON
bool CategoryManager::readFromJson() {
    try {
        QJsonObject json = this->readJsonFromPath(this->getSourcePath());
        QJsonArray categoriesJson = json.value("categories").toArray();
        foreach(const QJsonValue &entry, categoriesJson) {
            if(entry.toString() != "assets"){
                CategoryModel * category = new CategoryModel();
                category->setSourcePath(entry.toString());
                if(!category->readFromJson()) {
                    qDebug() << "Unable to read category json: " << entry.toString();
                    continue;
                }
                this->addCategory(category);
            }
        }
    }
    catch(...) {
        return false;
    }
    return true;
}

bool CategoryManager::writeToJson() {
    QJsonObject json;

    // Save Category Paths:
    QJsonArray categoriesJson;
    foreach(const int categoryId, this->getCategoryIds()) {
        CategoryModel * category = this->getCategory(categoryId);
        if(category->internal)
            continue;
        categoriesJson.append(category->getSourcePath());
    }
    json.insert("categories", categoriesJson);

    return this->writeJsonToPath(this->getSourcePath(), json);
}

QString CategoryManager::getAbsolutePath(QString path) {
    if(QDir::isAbsolutePath(path))
        return path;
    return QFileInfo("categories.json").absoluteDir().path() + QDir::separator() + path;
}

// Categories
QHash<int, CategoryModel *> CategoryManager::getCategories()
{
    return this->categories;
}

QVector<int> CategoryManager::getCategoryIds()
{
    return this->categoryIds;
}

CategoryModel * CategoryManager::getCategory(int categoryId)
{
    return this->categories[categoryId];
}

bool CategoryManager::addCategory(CategoryModel * category)
{
    if(category->getId() >= 0)
    {
        if(this->categories.contains(category->getId()))
        {
            return false;
        }
    }
    else
    {
        category->setId(this->nextId++);
    }
    this->categories.insert(category->getId(), category);
    this->categoryIds.append(category->getId());
    return true;
}

CategoryModel * CategoryManager::createCategory()
{
    CategoryModel * category = new CategoryModel();
    this->addCategory(category);
    return category;
}

CategoryModel * CategoryManager::importCategory(QString path)
{
    CategoryModel * category = new CategoryModel();
    category->setSourcePath(path);
    category->readFromJson();
    this->addCategory(category);
    return category;
}

bool CategoryManager::removeCategory(int categoryId)
{
    if(!this->categories.contains(categoryId))
    {
        return false;
    }
    this->categories.remove(categoryId);
    this->categoryIds.removeOne(categoryId);
    return true;
}

void CategoryManager::clearCategories()
{
    this->categories.clear();
    this->categoryIds.clear();
}
