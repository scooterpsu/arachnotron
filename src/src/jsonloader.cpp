#include "include/jsonloader.h"

#include <QDebug>
#include <QDir>
#include <QTextStream>
#include <QJsonDocument>

// Constructor
JsonLoader::JsonLoader()
{
    this->id = -1;
}

QString JsonLoader::getModelType() {
    return "Model";
}


// Getters
int JsonLoader::getId() const {
    return this->id;
}

QString JsonLoader::getSourcePath() const {
    return this->sourcePath;
}

QString JsonLoader::getSourceDir() const
{
    return QFileInfo(this->getSourcePath()).absoluteDir().path();
}

QString JsonLoader::getPath(QString path) const
{
    if(QDir::isAbsolutePath(path)) {
        return path;
    }
    return  this->getSourceDir() + QDir::separator() + path;
}

QString JsonLoader::getAssetPath(QString key) const
{
    if(!this->strings.contains(key)) {
        return "";
    }
    if(this->internal) {
        return QString("..") + QDir::separator() + "assets" + QDir::separator() + this->getString(key);
    }
    QString path = this->getString(key);
    if(!QDir::isAbsolutePath(path)) {
        path = this->getSourceDir() + QDir::separator() + path;
    }
    return "file:///" + path;
}

QString JsonLoader::getString(QString key) const {
    if(!this->strings.contains(key)) {
        return "";
    }
    return this->strings[key];
}

double JsonLoader::getDouble(QString key) const {
    if(!this->doubles.contains(key)) {
        return 0;
    }
    return this->doubles[key];
}

bool JsonLoader::getBool(QString key) const {
    if(!this->bools.contains(key)) {
        return false;
    }
    return this->bools[key];
}

QVector<QString> JsonLoader::getList(QString key) const {
    return this->lists[key];
}

QHash<QString, QString> JsonLoader::getMap(QString key) const {
    return this->maps[key];
}

QString JsonLoader::getMapValue(QString mapKey, QString valueKey) const {
    return this->maps[mapKey][valueKey];
}

QList<QString> JsonLoader::getMapKeys(QString key) const {
    return this->maps[key].keys();
}


// Setters
void JsonLoader::setId(int id) {
    this->id = id;
}

void JsonLoader::setSourcePath(QString value) {
    this->sourcePath = value;
}

void JsonLoader::setString(QString key, QString value) {
    this->strings.insert(key, value);
}

void JsonLoader::setDouble(QString key, double value) {
    this->doubles.insert(key, value);
}

void JsonLoader::setBool(QString key, bool value) {
    this->bools.insert(key, value);
}

void JsonLoader::setList(QString key, QVector<QString> value) {
    this->lists.insert(key, value);
}


// Lists
void JsonLoader::addListValue(QString key, QString value) {
    this->lists[key].append(value);
}

bool JsonLoader::removeListValue(QString key, QString value) {
    if(!this->lists[key].contains(value)) {
        return false;
    }
    this->lists[key].removeOne(value);
    return true;
}


// Maps
void JsonLoader::addMapValue(QString mapKey, QString key, QString value) {
    this->maps[mapKey][key] = value;
}

bool JsonLoader::removeMapValue(QString mapKey, QString key) {
    if(!this->maps[mapKey].contains(key)) {
        return false;
    }
    this->maps[mapKey].remove(key);
    return true;
}

void JsonLoader::clearMap(QString key) {
    this->maps[key].clear();
}


// JSON
bool JsonLoader::readFromJson() {
    try {
        this->fromJson(this->readJsonFromPath(this->getSourcePath()));
    }
    catch(...) {
        return false;
    }
    return true;
}

bool JsonLoader::writeToJson() {
    try {
        this->writeJsonToPath(this->getSourcePath(), this->toJson());
    }
    catch(...) {
        return false;
    }
    return true;
}

void JsonLoader::fromJson(QJsonObject json) {
    foreach(const QString &key, json.keys()) {
        this->parseJson(key, json.value(key));
    }
}

void JsonLoader::parseJson(QString key, QJsonValue value) {
    // String
    if(value.isString()) {
        this->setString(key, value.toString());
    }

    // Double
    else if(value.isDouble()) {
        this->setDouble(key, value.toDouble());
    }

    // Bool
    else if(value.isBool()) {
        this->setBool(key, value.toBool());
    }

    // Array
    else if(value.isArray()) {
        QJsonArray jsonArray = value.toArray();
        foreach(const QJsonValue &arrayValue, jsonArray) {

            // List
            if(arrayValue.isString()) {
                this->addListValue(key, arrayValue.toString());
            }

            // Map
            else if(arrayValue.isObject()) {
                QJsonObject subJson = arrayValue.toObject();
                this->addMapValue(key, subJson.value("name").toString(), subJson.value("value").toString());
            }
        }
    }
}

QJsonObject JsonLoader::toJson() {
    QJsonObject json;

    // Strings
    foreach(const QString &key, this->strings.keys()) {
        json.insert(key, this->strings[key]);
    }

    // Doubles
    foreach(const QString &key, this->doubles.keys()) {
        json.insert(key, this->doubles[key]);
    }

    // Bools
    foreach(const QString &key, this->bools.keys()) {
        json.insert(key, this->bools[key]);
    }

    // Lists
    foreach(const QString &key, this->lists.keys()) {
        QJsonArray jsonArray;
        foreach(const QString &value, this->lists[key]) {
            jsonArray.append(value);
        }
        json.insert(key, jsonArray);
    }

    // Maps
    foreach(const QString &mapKey, this->maps.keys()) {
        QJsonArray jsonArray;
        foreach(const QString &key, this->maps[mapKey].keys()) {
            QJsonObject subJson;
            subJson.insert("name", key);
            subJson.insert("value", this->maps[mapKey][key]);
            jsonArray.append(subJson);
        }
        json.insert(mapKey, jsonArray);
    }

    return json;
}

QJsonObject JsonLoader::readJsonFromPath(QString path) {
    QString data = this->readStringFromPath(path);
    if(data == "") {
        qDebug() << "Json file was empty or missing.";
        throw std::runtime_error("Json file was empty or missing.");
    }
    QJsonDocument json = QJsonDocument::fromJson(data.toUtf8());
    return json.object();
}

bool JsonLoader::writeJsonToPath(QString path, QJsonObject json) {
    qDebug() << "Saving json to: " + path;
    QJsonDocument document(json);
    QString data = QString::fromUtf8(document.toJson(QJsonDocument::Indented));
    this->writeStringToPath(path, data);
    return true;
}


// Strings
QString JsonLoader::readStringFromPath(QString path) {
    QFile file(path);
    if(!file.exists())
        return "";
    QTextStream stream(&file);
    QString data;
    if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        while(!stream.atEnd()) {
            data = stream.readAll();
        }
    }
    file.close();
    return data;
}

bool JsonLoader::writeStringToPath(QString path, QString data) {
    QFile file(path);
    if(file.open(QIODevice::ReadWrite | QIODevice::Truncate)) {
        QTextStream stream(&file);
        stream << data << endl;
    }
    return true;
}
