#include "include/profilebase.h"
#include "include/profilemanager.h"

#include <cmath>
#include <QDir>

ProfileBase::ProfileBase() : JsonLoader()
{
    ProfileManager::instance->qmlEngine->setObjectOwnership(this, QQmlEngine::CppOwnership);
}

// Launch
void ProfileBase::addArg(QTextStream &argStream, QString value)
{
    if(value == "")
        return;
    argStream << QString(" ") << value;
}

void ProfileBase::addArg(QTextStream &argStream, QString key, QString value)
{
    if(value == "")
        return;
    argStream << QString(" ") << key;
    if(key != "" && key != "-")
        argStream << QString(" ");
    argStream << value;
}

void ProfileBase::addArgQuotes(QTextStream &argStream, QString key, QString value)
{
    if(value == "")
        return;
    this->addArg(argStream, key, "\"" + value + "\"");
}

void ProfileBase::addArg(QTextStream &argStream, QString key, bool value)
{
    if(!value)
        return;
    if(key != "")
        argStream << QString(" ") << key;
}

void ProfileBase::addArg(QTextStream &argStream, QString key, int value)
{
    if(key != "")
        argStream << QString(" ") << key;
    argStream << QString(" ") << value;
}

void ProfileBase::addArg(QTextStream &argStream, QString key, double value)
{
    this->addArg(argStream, key, int(round(value)));
}

void ProfileBase::addArg(QTextStream &argStream, QString key, double value, double minThreshold)
{
    if(value <= minThreshold) {
        return;
    }
    this->addArg(argStream, key, value);
}

