#include "include/profilemodel.h"
#include "include/profilemanager.h"
#include "include/settingsmanager.h"

#include <QDir>
#include <QProcess>
#include <QtDebug>

// Constructor
ProfileModel::ProfileModel() : ProfileBase()
{
    this->id = -1;
    this->setString("name", "New Profile");
}

QString ProfileModel::getModelType() {
    return "Profile";
}

QList<ProfileModel *> ProfileModel::getInheritedProfiles() const {
    QList<ProfileModel *> inheritedProfiles;
    for(QString inheritedProfileName : this->getList("inheritProfiles")) {
        ProfileModel * inheritedProfile = ProfileManager::instance->getProfile(inheritedProfileName);
        if(inheritedProfile && inheritedProfile != this) {
            inheritedProfiles.append(inheritedProfile);
        }
    }
    return inheritedProfiles;
}


// Getters
QString ProfileModel::getInheritedString(QString key) const {
    QString value = this->getString(key);
    for(ProfileModel * inheritedProfile : this->getInheritedProfiles()) {
        value = inheritedProfile->getString(key) + " " + value;
    }
    return value;
}

QVector<QString> ProfileModel::getInheritedList(QString key) const {
    QVector<QString> value = this->getList(key);
    if(key.compare("inheritProfiles") == 0) {
        return value;
    }
    for(ProfileModel * inheritedProfile : this->getInheritedProfiles()) {
        QVector<QString> inheritedValue;
        inheritedValue.append(inheritedProfile->getList(key));
        inheritedValue.append(value);
        value = inheritedValue;
    }
    return value;
}

QHash<QString, QString> ProfileModel::getInheritedMap(QString key) const {
    QHash<QString, QString> value;
    value.unite(this->getMap(key));
    for(ProfileModel * inheritedProfile : this->getInheritedProfiles()) {
        QHash<QString, QString> inheritedValue = inheritedProfile->getMap(key);
        for(QString mapKey : inheritedValue.keys()) {
            value.insert(mapKey, inheritedValue[mapKey]);
        }
    }
    return value;
}

QString ProfileModel::getInheritedMapValue(QString mapKey, QString valueKey) const {
    return this->getInheritedMap(mapKey)[valueKey];
}

QList<QString> ProfileModel::getInheritedMapKeys(QString key) const {
    return this->getInheritedMap(key).keys();
}


// JSON
bool ProfileModel::readFromJson() {
    if(!this->ProfileBase::readFromJson()) {
        return false;
    }
    this->getLaunchSettings()->readFromJson();
    return true;
}

bool ProfileModel::writeToJson() {
    if(!this->ProfileBase::writeToJson()) {
        return false;
    }
    this->getLaunchSettings()->writeToJson();
    return true;
}

// Categories
bool ProfileModel::isInCategory(QString categoryName)
{
    return this->getList("categories").contains(categoryName);
}

// Launch
bool ProfileModel::launch()
{
    QString launchCommand = this->getLaunchCommand();
    if(launchCommand.compare("") == 0) {
        return false;
    }
    qDebug() << "Launch Command: " << launchCommand;

    QProcess * process = new QProcess();
    process->start(launchCommand);
    if(process->state() != QProcess::Running) {
        qDebug() << "Error: " << process->errorString();
    }

    return true;
}

QString ProfileModel::getLaunchCommand() {
    QString args;
    QTextStream out(&args);

    // Engine and Config:
    QString engineName = this->getString("engine");
    QPair<QString, QString> engineData = SettingsManager::instance->getEngine(engineName);
    if(engineData.first == "" && engineData.first == "") {
        qDebug() << "Engine " << engineName << " isn't added.";
        return "";
    }
    if(engineData.first == "") {
        qDebug() << "Engine " << engineName << " path not set.";
        return "";
    }
    this->addArgQuotes(out, "-config", engineData.second);

    // Iwad:
    QString iwad = SettingsManager::instance->getIwad(this->getString("iwad"));
    if(iwad == "") {
        qDebug() << "Missing IWAD";
        return "";
    }
    this->addArgQuotes(out, "-iwad", iwad);

    // Resources:
    foreach(QString resource, this->getInheritedList("resources")) {
        this->addArgQuotes(out, "-file", this->getPath(resource));
    }

    // Launch Args:
    this->addArg(out, this->getInheritedString("args"));
    out << this->getLaunchSettings()->getLaunchArgs();

    // Global Settings Args:
    this->addArg(out, "-noidle", SettingsManager::instance->getBool("noidle"));

    // Cvars:
    QHash<QString, QString> cvarMap = this->getInheritedMap("cvars");
    foreach(QString cvarKey, cvarMap.keys()) {
        QString cvarValue = cvarMap[cvarKey];
        QString launchCvarValue = this->getLaunchSettings()->getMapValue("cvars", cvarKey);
        if(launchCvarValue != "")
            cvarValue = launchCvarValue;

        this->addArg(out, "+set " + cvarKey + " " + cvarValue, true);
    }

    QString launchCommand = "\"" + engineData.first + "\"" + args;
    return launchCommand;
}

ProfileLaunch * ProfileModel::getLaunchSettings()
{
    if(this->launchSettings == nullptr) {
        this->launchSettings = new ProfileLaunch(this);
    }
    return this->launchSettings;
}
