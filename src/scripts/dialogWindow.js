.pragma library

// This script controls dialogs (small overlay windows). There can only be one dialog active and it is dispalyed over popups.

// Dialog Target:
var dialogTarget = null;
function setDialogTarget(target) {
    dialogTarget = target;
}

var dialogBlur = null;
function setDialogBlur(target) {
    dialogBlur = target;
}

var dialogForm = null;
function setDialogForm(formQml) {
    // Remove Existing Form:
    if(dialogForm !== null) {
        //if(dialogForm.type !== undefined)
            dialogForm.destroy();
        dialogForm = null;
    }

    // No Form:
    if(formQml === "") {
        dialogBlur.visible = false;
        dialogBlur.enabled = false;
        return;
    }

    // Create New Form:
    var component = Qt.createComponent(formQml);
    dialogForm = component.createObject(dialogTarget);
    if(dialogForm === null) {
        console.log("Error creating dialog form: " + formQml);
        return;
    }
    dialogForm.anchors.top = dialogForm.parent.top;
    dialogForm.anchors.bottom = dialogForm.parent.bottom;
    dialogForm.anchors.left = dialogForm.parent.left;
    dialogForm.anchors.right = dialogForm.parent.right;
    dialogBlur.visible = true;
    dialogBlur.enabled = true;
}
