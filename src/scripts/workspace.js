.pragma library

// This script manages the workspace (the main large top right area).

var workspaceTarget = null;
function setWorkspaceTarget(target) {
    workspaceTarget = target;
}

var workspace = null;
function setWorkspace(formQml) {
    // Remove Existing Form:
    if(workspace !== null) {
        workspace.destroy();
        workspace = null;
    }

    // No Form:
    if(formQml === "") {
        return null;
    }

    // Create New Form:
    var component = Qt.createComponent(formQml);
    workspace = component.createObject(workspaceTarget);
    if(workspace === null) {
        console.log("Error creating workspace: " + formQml);
        return null;
    }
    workspace.anchors.top = workspace.parent.top;
    workspace.anchors.bottom = workspace.parent.bottom;
    workspace.anchors.left = workspace.parent.left;
    workspace.anchors.right = workspace.parent.right;

    return workspace;
}
