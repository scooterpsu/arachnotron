.pragma library
.import "workspace.js" as Workspace
.import "controls.js" as Controls

// This script manages categories as a whole.
var categoryManager;
var selectedCategory = null;
var selectedCategoryButton = null;
var reselectCategory = true;
var categoryEditor = null;


// Category Manager:
function setCategoryManager(manager) {
    categoryManager = manager;
}


// Category List:
var categoryListTarget;
function setCategoryListTarget(target) {
    categoryListTarget = target;

    for(var categoryId in categoryManager.getCategoryIds()) {
        var category = categoryManager.getCategory(categoryId);
        addCategoryButton(category);
    }
}


// ========== Category Button ==========
// Add Category Button:
var categoryButtonForm = "/qml/categoryButton.qml";
function addCategoryButton(category) {
    var id = category.getId();
    var name = category.getString("name");
    var icon = category.getAssetPath("iconPath");

    // Create Button:
    var component = Qt.createComponent(categoryButtonForm);
    var entry = component.createObject(categoryListTarget);
    if(entry === null) {
        console.log("Error creating category button form: " + categoryButtonForm);
        return null;
    }
    entry.anchors.left = entry.parent.left;
    entry.anchors.right = entry.parent.right;

    entry.categoryId = parseInt(id);
    entry.name.text = name;
    entry.icon.source = icon;

    return entry;
}

// Deselect Category Button:
function deselectCategoryButton() {
    if(selectedCategoryButton !== null) {
        selectedCategoryButton.selectedEffect.visible = false;
    }
}


// ========== Category Control ==========
// Remove Category:
function removeCategory() {
    if(selectedCategoryButton !== null) {
        selectedCategoryButton.destroy();
        selectedCategoryButton = null;
    }

    Workspace.setWorkspace("");
    Controls.setControls("");

    if(selectedCategory) {
        categoryManager.removeCategory(selectedCategory.getId());
        selectedCategory = null;
        categoryManager.writeToJson();
    }

}


// Select Category:
function selectCategory(categoryButton) {
    if(!categoryButton) {
        selectedCategory = null;
        if(selectedCategoryButton !== null) {
            //selectedCategoryButton.selectedEffect.visible = false;
        }
        selectedCategoryButton = null;
        return;
    }

    if(!reselectCategory && selectedCategory && categoryButton.categoryId === selectedCategory.getId()) {
        return;
    }

    reselectCategory = false;
    selectedCategory = categoryManager.getCategory(categoryButton.categoryId);

    Workspace.setWorkspace("../qml/categoryView.qml");
    Controls.setControls("../qml/categoryViewControls.qml");

    // Button:
    //deselectCategoryButton();
    selectedCategoryButton = categoryButton;
    //selectedCategoryButton.selectedEffect.visible = true;
}


// Create Category:
function createCategory() {
    selectCategory();
    editCategory();
}


// Edit Category:
function editCategory() {
    reselectCategory = true;
    categoryEditor = Workspace.setWorkspace("../qml/categoryEdit.qml");
    Controls.setControls("../qml/categoryEditControls.qml");
}


// Save Category:
function saveCategory() {
    // New Category:
    if(!selectedCategory) {
        selectedCategory = categoryManager.createCategory();
    }

    //Don't try to save changes to "All"
    if(selectedCategory.getString("name") !== "All"){
        // Apply Save Data:
        if(categoryEditor) {
            categoryEditor.saveData(selectedCategory);
            selectedCategory.writeToJson();
            categoryManager.writeToJson();

            // Update Button:
            if(selectedCategoryButton) {
                selectedCategoryButton.name.text = selectedCategory.getString("name");
                selectedCategoryButton.icon.source = selectedCategory.getAssetPath("iconPath");
            }
            else {
                selectedCategoryButton = addCategoryButton(selectedCategory);
            }
        }
    }

    selectCategory(selectedCategoryButton);
}

// Import
function onImportCategory(importedCategory) {
    if(!importedCategory)
            return;
    var button = addCategoryButton(importedCategory);
    return button;
}
