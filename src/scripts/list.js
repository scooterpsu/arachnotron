.import "fileLoader.js" as FileLoader

var entries = [];
var listType;
var includeName = false;

function load(list, key, source, entryQML, mode, modeData, inherited) {
    list.key = key;
    list.source = source;
    list.entryQML = entryQML;
    list.mode = mode;
    list.modeData = modeData;
    list.sourceInherited = inherited === true;

    if(mode === "files") {
        list.rootDirectoryInput = modeData;
    }

    if(mode === "launchCvars") {
        list.addButton.visible = false;
        list.resetButton.visible = true;
        includeName = true;
    }

    loadEntries(list);
}

function loadFromList(list, key, source, entryQML, mode, modeData, inherited) {
    listType = "list";
    load(list, key, source, entryQML, mode, modeData, inherited);
}

function loadFromMap(list, key, source, entryQML, mode, modeData, inherited) {
    listType = "map";
    load(list, key, source, entryQML, mode, modeData, inherited);
}

function loadEntries(list) {
    if(list.source) {
        var listData;
        if(listType === "list") {
            if(!list.sourceInherited)
                listData = list.source.getList(key);
            else
                listData = list.source.getInheritedList(key);
        }
        else if(listType === "map") {
            if(!list.sourceInherited)
                listData = list.source.getMapKeys(key);
            else
                listData = list.source.getInheritedMapKeys(key);
        }
        for(var i in listData) {
            addEntry(list, listData[i]);
        }
    }
}

function reset(list) {
    for(var i in entries) {
        entries[i].remove(list);
        entries[i].destroy();
    }
    entries = [];
    loadEntries(list);
}

function addEntry(list, value) {
    // No Form:
    if(!list.entryQML) {
        return null;
    }

    // Create New Form:
    var component = Qt.createComponent(list.entryQML);
    var entry = component.createObject(list.listContainer);
    if(!entry) {
        console.log("Error creating list entry form: " + list.entryQML);
        return null;
    }
    entry.anchors.left = entry.parent.left;
    entry.anchors.right = entry.parent.right;
    entry.load(entry, list, value);

    // Files List:
    if(entry.rootDirectoryInput !== undefined) {
        entry.rootDirectoryInput = list.rootDirectoryInput;
    }

    entries.push(entry);
    return entry;
}

function removeEntry(target) {
    var index = entries.indexOf(target);
    if(index >= 0)
        entries.splice(index, 1);
    target.destroy();
}

function moveEntryUp(target) {
    var childIndex = Array.prototype.indexOf.call(target.parent.children, target);
    if(childIndex === 0) {
        return;
    }
    var swapTarget = target.parent.children[childIndex - 1];

    for(var i = 0; i < target.inputFields.length; i++) {
        var swapInput = getFieldValue(swapTarget.inputFields[i]);
        setFieldValue(swapTarget.inputFields[i], getFieldValue(target.inputFields[i]));
        setFieldValue(target.inputFields[i], swapInput);
    }
}

function moveEntryDown(target) {
    var childIndex = Array.prototype.indexOf.call(target.parent.children, target);
    if(childIndex === target.parent.children.length - 1) {
        return;
    }
    var swapTarget = target.parent.children[childIndex + 1];

    for(var i = 0; i < target.inputFields.length; i++) {
        var swapInput = getFieldValue(swapTarget.inputFields[i]);
        setFieldValue(swapTarget.inputFields[i], getFieldValue(target.inputFields[i]));
        setFieldValue(target.inputFields[i], swapInput);
    }
}

function getFieldValue(inputField) {
    var value = inputField.text;
    if(value) {
        return value;
    }
    return inputField.currentText;
}

function setFieldValue(inputField, value) {
    var currentValue = inputField.text;
    if(currentValue) {
        inputField.text = value;
        return;
    }
    inputField.currentText = value;
}

function onFileDrop(list, inputUrls) {
    var urls = FileLoader.getCleanUrls(inputUrls);
    if(mode === "files") {
        for(var i in urls) {
            addEntry(list, urls[i]);
        }
    }
    else if(mode === "iwads" || mode === "engines") {
        for(var j in urls) {
            addEntry(list, [urls[j]]);
        }
    }
}

// Save Data:
function getSaveData() {
    var saveData = [];
    for(var i in entries) {
        if(entries[i].inputFields.length > 1 || includeName) {
            saveData[i] = [];
            var offset = 0;
            if(includeName) {
                saveData[i][offset] = entries[i].name.text;
                offset++;
            }
            for(var j = 0; j < entries[i].inputFields.length; j++) {
                saveData[i][j + offset] = getFieldValue(entries[i].inputFields[j]);
            }
            continue;
        }
        saveData[i] = getFieldValue(entries[i].inputFields[0]);
    }
    return saveData;
}
