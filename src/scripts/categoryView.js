.pragma library
.import "categories.js" as Categories

// This script manages the Category View.


// Profile List:
var profileListTarget;
function setProfileListTarget(target) {
    profileListTarget = target;

    var profileCount = Categories.selectedCategory.loadProfiles();
    for(var i = 0; i < profileCount; i++) {
        var profile = Categories.selectedCategory.getProfile(i);
        if(profile === null) {
            console.log("Null Profile!");
            continue;
        }
        addProfile(profile.getId(), profile.getString("name"), profile.getAssetPath("iconPath"), profile.getBool("backgroundTileHide"));
    }
}


// Add Profile:
var profileButtonForm = "/qml/profileButton.qml";
function addProfile(id, name, icon, hideBackground) {
    // Create Button:
    var component = Qt.createComponent(profileButtonForm);
    var entry = component.createObject(profileListTarget);
    if(entry === null) {
        console.log("Error creating profile button form: " + profileButtonForm);
        return null;
    }

    entry.profileId = parseInt(id);
    entry.name.text = name;
    entry.icon.source = icon;
    entry.background.source = hideBackground ? "" : entry.backgroundDefault;

    return entry;
}
