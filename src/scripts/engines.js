.pragma library
.import "settings.js" as Settings
.import "listLoader.js" as ListLoader

// This script is for engines.
var enginesList = null;


// Save:
function saveSettings() {
    var saveData = enginesList.getSaveData();
    Settings.settingsManager.clearEngines();
    for(var i in saveData) {
         Settings.settingsManager.addEngine(saveData[i][0], saveData[i][1], saveData[i][2]);
    }
    Settings.settingsManager.writeToJson();
}


// Lists:
function loadEnginesList(target) {
    enginesList = ListLoader.loadList("../qml/list.qml", target);
    enginesList.entryQML = "../qml/listEntryEngine.qml";
    enginesList.mode = "engines";

    var engines = Settings.settingsManager.getEngineKeys();
    for(var i in engines) {
        var entry = enginesList.addEntry(enginesList, engines[i]);
    }
}
